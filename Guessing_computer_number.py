
import random

def guess(minnumber ,maxnumber):
    
    number=random.randint(minnumber,maxnumber)
   
    guess_number=0.1
    print(f'Clue : The number is between {minnumber} and {maxnumber}')
    while number!=guess_number:
        guess_number=int(input('Guess the number picked by the computer. '))
    
        if number>guess_number:
            print('Wrong! Try a bigger number')
        elif number<guess_number:
        
            print("Wrong! Try a smaller number")
        else:
            print('Congratulations you have guessed correctly')
            pass
            
decision = True
while decision==True:       
    print('''Lets play a guessing game. 
First give the computer the range of values to choose from then try to guess the number it picked''')
    minnumber= int(input("Enter the lower limit value :"))
    maxnumber= int(input('Enter the upper limit value :'))
    guess(minnumber ,maxnumber)
    
    prompt=input("Press \n Y  if you want to continue   \n  N  if you want to quit : ")
    if prompt in ('y','Y'):
        decision=True
    elif prompt in ('n','N'):
        decision=False
    else:
        print('Invalid entry')
        decision=False

        
